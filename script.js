import {afficher} from "./javascript/afficher.js";

document.addEventListener("DOMContentLoaded", function () {
  if (navigator.onLine) {
    document.querySelector(".notification").setAttribute("hidden", "");
  }

  window.addEventListener("online", () => {
    document.querySelector(".notification").setAttribute("hidden", "");
  });
  window.addEventListener("offline", () => {
    document.querySelector(".notification").removeAttribute("hidden");
  });

  let fetchData;
  if (navigator.onLine) {
    fetchData = fetch("/images.json")
      .then((response) => response.json())
      .then((data) => localforage.setItem("data", data));
  } 
  else {
    fetchData = localforage.getItem("data");
  }
  fetchData.then((json) => afficher(json));
});
