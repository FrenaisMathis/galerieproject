export function countLike() {
  let getLikes;
  if (navigator.onLine) {
    getLikes = fetch("http://localhost:3000/likes")
      .then((response) => response.json())
      .then((likes) => {
        return localforage.setItem("likes", likes);
      });
  } else {
    getLikes = localforage.getItem("likes");
  }

  getLikes.then((allLikes) => {
    const likes = allLikes || [];

    likes.forEach((id) => {
      $("i[data-like-id='" + id + "']").addClass("is-like");
      $("i[data-like-id='" + id + "']").addClass("fas");
      $("i[data-like-id='" + id + "']").removeClass("far");
    });
  });
}

export function clickLike() {
  const likesButton = $("i[data-like-id]");
  Array.from(likesButton).forEach((link) => {
    link.addEventListener("click", function (event) {
      localforage
        .getItem("likes")
        .then((storedlikes) => {
          const likes = storedlikes || [];
          const likeId = event.target.dataset.likeId;
          let newlikes;
          if (event.target.classList.contains("is-like")) {
            newlikes = likes.filter((id) => id !== likeId);
          } else {
            newlikes = Array.from(new Set([...likes, likeId]));
          }
          if (navigator.onLine) {
            localforage.setItem("likes", newlikes);
            return fetch("http://localhost:3000/likes", {
              method: "POST",
              headers: {
                "Content-Type": "application/json"
              },
              body: JSON.stringify(newlikes),
            });
          } else {
            return localforage.setItem("likes", newlikes).then(() => {
              navigator.permissions
                .query({
                  name: "background-sync",
                })
                .then(({
                  state
                }) => {
                  console.log(state);
                  if (state === "granted") {
                    Notification.requestPermission(function (status) {
                      console.log("Notification permission status:", status);
                    });

                    return navigator.serviceWorker.ready.then((reg) => {
                      return reg.sync.register("syncLikes");
                    });
                  }
                });
            });
          }
        })
        .then(() => {
          event.target.classList.toggle("is-like");
          event.target.classList.toggle("fas");
          event.target.classList.toggle("far");

        });
    });
  });
}