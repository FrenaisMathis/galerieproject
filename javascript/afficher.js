import {countLike, clickLike} from "./like.js";
import {reduireArray, dateTimeFormat } from "./generique.js";

export function afficher(json){
	const selections = reduireArray(json, 3);

  let html = "";

  html += '<div class="row">';
  selections.forEach(selection => {

    selection.forEach((repo) => {
      html += `
          <div class="col-md-6 col-12 my-2">
            <div class="card">
              <div class="card-image">
                <figure class="image is-4by3">
                  <img class="w-100"
                    src="${repo.url}"
                    alt="${repo.name}"
                  />
                </figure>
              </div>
              <div class="card-content p-2">
                <div class="media">
                  <div class="media-content">
                    <h2>${repo.name}</h2>
                  </div>
                </div>
                <div class="content p-2 row">
                  <div class="col-8">
                    <p>${repo.description}</p>
                  </div>
                  <div class="col-4 text-right">
                    <button class="like-button" type="button">
                      <i class="far fa-heart" data-like-id=${repo.id}></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>`;
    });
  });
  html += "</div>";

  document.querySelector(".container").innerHTML = html;
  countLike();
  clickLike();
}