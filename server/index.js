const express = require("express");
const bodyParser = require("body-parser");

const cors = require("cors");

const app = express();
app.use(bodyParser.json());

app.use(cors());

const port = 3000;

let likes = [];

app.get("/likes", (request, response) => {
  response.send(likes);
});

app.post("/likes", (request, response) => {
  console.log(request.body);
  likes = request.body;
  response.send(likes);
});

app.listen(port, err => {
  console.log(`Server is listening on ${port}`);
});
